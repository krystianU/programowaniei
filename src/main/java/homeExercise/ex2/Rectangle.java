package homeExercise.ex2;

public class Rectangle implements Figure {
    private double sideA;
    private double sideB;
    private double perimeter;
    private double area;

    public Rectangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getPerimeter() {
        perimeter = sideA * sideB;
        return perimeter;
    }

    @Override
    public double getArea() {
        area = 2 * sideA + 2* sideB;
        return area;
    }

    public Rectangle getRectangle(){
        return this;
    }
    public void countPerimeterAndArea(){
        getPerimeter();
        getArea();
    }
}
