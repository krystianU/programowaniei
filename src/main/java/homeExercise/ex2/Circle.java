package homeExercise.ex2;

public class Circle implements Figure {

    private double radius;
    private final double PI = Math.PI;
    private double perimeter;
    private double area;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        perimeter = PI * Math.pow(radius, 2);
        return perimeter;
    }

    @Override
    public double getArea() {
        area = 2 * PI * radius;
        return area;
    }

    public Circle getCircle() {
        return this;
    }

    public void countPerimeterAndArea() {
        getPerimeter();
        getArea();
    }
}
