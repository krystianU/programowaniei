package homeExercise.ex2;

public class Triangle implements Figure {
    private double sideA;
    private double sideB;
    private double sideC;
    private double perimeter;
    private double area;

    public Triangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getPerimeter() {
        perimeter = sideA * sideB / 2;
        return perimeter;
    }

    @Override
    public double getArea() {
        sideC = Math.pow(sideA, 2) + Math.pow(sideB, 2);
        area = sideA + sideB + sideC;
        return area;
    }

    public void countPerimeterAndArea() {
        getPerimeter();
        getArea();
    }

    public Triangle getTriangle() {
        return this;
    }
}
