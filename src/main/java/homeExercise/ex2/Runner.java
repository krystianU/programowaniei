package homeExercise.ex2;

public class Runner {
    ScannerImplementation scannerImplementation = new ScannerImplementation();
    private Circle circle;
    private Triangle triangle;
    private Rectangle rectangle;
    Figure[] figures = new Figure[3];

    public void printFigures() {
        for (Figure figures1 : figures) {
            System.out.print(figures1.getPerimeter() + ", " + figures1.getArea());
            System.out.println();
        }
    }

    public void runCircle() {
        circle = new Circle(scannerImplementation.inputValue());
        circle.countPerimeterAndArea();
        figures[0] = circle.getCircle();

    }

    public void runTriangle() {
        triangle = new Triangle(scannerImplementation.inputValue(), scannerImplementation.inputValue());
        triangle.countPerimeterAndArea();
        figures[1] = triangle.getTriangle();
    }

    public void runRectangle() {
        rectangle = new Rectangle(scannerImplementation.inputValue(), scannerImplementation.inputValue());
        rectangle.countPerimeterAndArea();
        figures[2] = rectangle.getRectangle();
    }

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.runCircle();
        runner.runTriangle();
        runner.runRectangle();
        runner.printFigures();
    }
}
