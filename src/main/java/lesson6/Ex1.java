package lesson6;

import java.util.*;
import java.util.Collections;
import java.util.List;

/*
Przy pomocy prostej listy:
List<String> names = Arrays.asList("Kasia", "Ania", "Zosia", "Bartek");
Oraz:
Collections.sort(names, new Comparator<String>() {
@Override
public int compare(String o1, String o2) {
return o1.compareToIgnoreCase(o2);
}
});
Zamień metodę sortowania tak by użyła lambdy, sprawdź przypadki gdy dodasz więcej imion
do listy.
Dodatkowo napisz tak metodę sortującą aby posortowała imiona w odwrotną stronę.
 */
public class Ex1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Kasia", "Ania", "Zosia", "Bartek");
//        Collections.sort(names, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.compareToIgnoreCase(o2);
//            }
//        });
       Collections.sort(names, (o1, o2) -> o1.compareToIgnoreCase(o2));
    }
}
