package lesson6.exampleLambdaUsage;

public class Main {

    public static void main(String[] args) {

        Item item = new Item.Builder()
                .setValue1(10)
                .setValue2(20)
                .setValue1(2300)
                .setValue2(10000)
                .setValue1(10)
                .setValue1ByListener(new ItemOperations() {
                    @Override
                    public int doOperations(int value1, int value2) {
                        return value1 * value2;
                    }
                })
                .setValue1ByListener(new ItemOperations() {
                    @Override
                    public int doOperations(int value1, int value2) {
                        return value1 * value2;
                    }
                })
                .setValue1ByListener(new ItemOperations() {
                    @Override
                    public int doOperations(int value1, int value2) {
                        return value1 * value2;
                    }
                })
                .setValue1ByListener(new ItemOperations() {
                    @Override
                    public int doOperations(int value1, int value2) {
                        return value1 * value2;
                    }
                })
                .setValue1ByListener(new ItemOperations() {
                    @Override
                    public int doOperations(int value1, int value2) {
                        return value1 * value2;
                    }
                })
                .getObject();

        Item item1 = new Item.Builder()
                .setValue1(10)
                .setValue2(20)
                .setValue1(2300)
                .setValue2(10000)
                .setValue1(10)
                .setValue1ByListener((value1, value2) -> value1 * value2)
                .setValue1ByListener((value1, value2) -> value1 * value2)
                .setValue1ByListener((value1, value2) -> value1 * value2)
                .setValue1ByListener((value1, value2) -> value1 * value2)
                .setValue1ByListener((value1, value2) -> value1 * value2)
                .getObject();
    }
}
