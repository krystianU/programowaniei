package lesson6.exampleLambdaUsage;

public class Item {

    private int value1;
    private int value2;

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    public static class Builder {
        //konieczne
        private Item item;
        //konieczne
        public Builder() {
            item = new Item();
        }
        //to co się dzieje pomiędzy, zależy od nas
        public Builder setValue1(int param) {
            item.setValue1(param);
            return this;
        }

        public Builder setValue2(int param) {
            item.setValue2(param);
            return this;
        }

        public Builder setValue1ByListener(ItemOperations operations) {
            item.setValue1(operations.doOperations(item.value1, item.value2));
            return this;
        }
        //konieczne
        public Item getObject() {
            return item;
        }
    }
}
