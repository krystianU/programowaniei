package lesson6.exampleLambdaUsage;

public interface ItemOperations {

    int doOperations(int value1, int value2);
}
