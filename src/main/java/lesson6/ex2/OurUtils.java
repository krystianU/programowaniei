package lesson6.ex2;

public class OurUtils {

    public static String betterString(String s1, String s2, StringComparator stringComparator) {
        if (stringComparator.compareString(s1, s2)) {
            return s1;
        } else {
            return s2;
        }
    }
}
