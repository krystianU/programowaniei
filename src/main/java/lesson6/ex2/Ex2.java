package lesson6.ex2;
import java.util.*;

/*
Celem tego zadania jest zrobienie metody o nazwie “betterString”, ma ona pobierać dwa
łańcuchy znaków i lambde, która określa, który z dwóch podanych ciągów jest lepszy.
Dodatkowo metoda ta powinna zwrócić odpowiednio lepszy ciąg znaków.
• String string1 = ...;
• String string2 = ...;
• String betterOne = OurUtils.betterString(string1, string2, (s1, s2) -> s1.length() >
s2.length());
Aby wykonać lambdę powinniśmy stworzyć odpowiedni interfejs zwracający wartość true/false,
zaś w samej klasie “OurUtils” powinniśmy zdecydować który łańcuch jest lepszy.
 */
public class Ex2 {

    public static void main(String[] args) {
        String string1 = "abcdsd";
        String string2 = "sdadsadsa";
        String betterOne = OurUtils.betterString(string1,string2, ((s1, s2) -> s1.length() > s2.length()));
        System.out.println(betterOne);

    }
}
