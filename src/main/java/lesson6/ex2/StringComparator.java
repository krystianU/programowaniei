package lesson6.ex2;

public interface StringComparator {

    boolean compareString(String s1, String s2);
}
