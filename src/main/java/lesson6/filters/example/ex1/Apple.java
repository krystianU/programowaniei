package lesson6.filters.example.ex1;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Apple {
    String taste;
    String color;
    int weight;

    public Apple(String taste, String color, int weight) {
        this.taste = taste;
        this.color = color;
        this.weight = weight;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(
                new Apple("good", "red", 1),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("good", "red", 2));

        double bestAppleWeight = 0;
//        Apple bestApple = null;
//        for (Apple apple : appleList) {
//            if (apple.taste.equals("good")) {
//                if (apple.color.equals("red")) {
//                    if (apple.weight == 1) {
//                        bestAppleWeight = apple.weight;
//                        bestApple = apple;
//                    }
//                }
//            }
//        }
//        System.out.println(bestApple);
        Optional<Apple> bestApple = appleList.stream()
                .filter(apple -> apple.taste.equals("good"))
                .filter(apple -> apple.color.equals("red"))
                .filter(apple -> apple.weight == 1)
                .findFirst();
        if (bestApple.isPresent()) {
            System.out.println(bestApple.get());
            bestAppleWeight = bestApple.get().weight;
        }
//        Solucja 1 (najcięższe jabłko)
//        Optional<Apple> lowestWeight = appleList.stream()
//                .sorted(Comparator.comparingInt(Apple::getWeight))
//                .findFirst();
        Optional<Apple> lowestWeight = appleList.stream()
                .max(Comparator.comparingInt(Apple::getWeight));

        Optional<Apple> lowestAndGreenApple = appleList.stream()
                .max(Comparator.comparingInt(Apple::getWeight));
//do skończenia
        try {
            lowestAndGreenApple.stream()
                    .filter(apple -> apple.color.equals("green"))
                    .findFirst();
        } catch (Exception AppleIsNotGreenException){
            lowestAndGreenApple.isEmpty();
            System.out.println("Apple is not green!");
        }

    }

}

