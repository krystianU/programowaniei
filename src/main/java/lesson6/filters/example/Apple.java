package lesson6.filters.example;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Apple {
    String taste;
    String color;
    int weight;

    public Apple(String taste, String color, int weight) {
        this.taste = taste;
        this.color = color;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "taste='" + taste + '\'' +
                ", color='" + color + '\'' +
                ", weight=" + weight +
                '}';
    }

    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(
                //przy takiej deklaracji można wpisywać elementy po przecinku
                new Apple("good", "red", 1),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("good", "red", 2));

        appleList.stream()
                .filter(apple -> apple.color.equals("red"))
                .filter(apple -> apple.taste.equals("good"))
                .filter(apple -> apple.weight > 1)
                //Wykonaj akcję dla każdej znalezionej wartości
                .forEach(apple -> System.out.println(apple.toString()));
    }
}
