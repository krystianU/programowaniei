// Wygwiazdkuj prostokąt o długości boków zadanych przez użytkownika.

import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();

        for (int i = 0; i < a ; i++){
            System.out.println();
            for (int k = 0; k < b; k++){
                System.out.print("*");
            }
        }
    }
}
