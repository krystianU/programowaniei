import java.util.Scanner;

class Ex3{
    private static void generateTree() {
        Scanner scanner = new Scanner(System.in);

        int treeHeight = scanner.nextInt();
        int branchHeight = scanner.nextInt();

        String branchBuildBeforeCenter = "";
        String branchBuildAfterCenter = "";

        for (int i = 0; i < treeHeight; i++) {
            int branchLength = i;

            branchBuildBeforeCenter = generateSpaceBeforeCenter(treeHeight, branchLength);

            for (int j = 0; j < branchLength; j++) {
                branchBuildBeforeCenter = branchBuildBeforeCenter + "*";
                branchBuildAfterCenter = branchBuildAfterCenter + "*";
            }

            for (int k = 0; k < branchHeight; k++) {
                printBranch(branchBuildBeforeCenter, branchBuildAfterCenter);
            }
            branchBuildAfterCenter = "";
            branchBuildBeforeCenter = "";
        }
    }

    private static String generateSpaceBeforeCenter(int treeHeight, int branchLength) {
        String branchBuildBeforeCenter = "";
        for (int k = 0; k < treeHeight - branchLength; k++) {
            branchBuildBeforeCenter = branchBuildBeforeCenter + " ";
        }
        return branchBuildBeforeCenter;
    }

    private static void printBranch(String branchBuildBeforeCenter,
                                    String branchBuildAfterCenter) {
        String branchCenter = "*";

        System.out.println(branchBuildBeforeCenter + branchCenter + branchBuildAfterCenter);
    }
    public static void main(String[] args) {
        generateTree();
    }
}


