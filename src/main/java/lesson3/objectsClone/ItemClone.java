package lesson3.objectsClone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemClone implements Cloneable {

    private int number1;
    private float number2;
    private char char1;
    private Apple apple;

    public ItemClone(int number1, float number2, char char1, Apple apple) {
        this.number1 = number1;
        this.number2 = number2;
        this.char1 = char1;
        this.apple = apple;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public float getNumber2() {
        return number2;
    }

    public void setNumber2(float number2) {
        this.number2 = number2;
    }

    public char getChar1() {
        return char1;
    }

    public void setChar1(char char1) {
        this.char1 = char1;
    }

    @Override
    public ItemClone clone() throws CloneNotSupportedException {
        ItemClone itemClone = (ItemClone) super.clone();
        itemClone.apple = (Apple) this.apple.clone();

        return itemClone;
    }

    public static void main(String[] args) throws CloneNotSupportedException {

        List<ItemClone> itemsList = Arrays.asList(new ItemClone(1, 12f, 'c', new Apple()));
        List<ItemClone> clonedItemsList = new ArrayList<>();


        for (int i = 0; i < itemsList.size(); i++) {
            clonedItemsList.add((ItemClone) itemsList.get(i).clone());
        }

        System.out.println("test");
    }
}
