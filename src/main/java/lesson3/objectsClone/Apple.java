package lesson3.objectsClone;


public class Apple implements Cloneable {
    String color;
    String taste;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Apple apple = new Apple();
        apple.setColor("red");
        apple.setTaste("good");
        Apple apple1 = (Apple) apple.clone();
        Apple apple2 = new Apple();
        apple2.setColor("red");
        apple2.setTaste("good");

        boolean check1 = apple == apple1;
        boolean check2 = apple1 == apple2;

        System.out.println("Porównanie1: " + check1);
        System.out.println("Porównanie2: " + check2);

        show(apple, apple1, apple2);
        apple1.setColor("green");
        apple2.setTaste("terrible");
        show(apple, apple1, apple2);
    }

    public static void show(Apple apple, Apple apple1, Apple apple2) {
        printApple(apple, 1);
        printApple(apple1, 2);
        printApple(apple2, 3);
    }


    public static void printApple(Apple apple, int number) {
        System.out.println("Jabłka " + number + " kolor " + apple.getColor() + "; smak " + apple.getTaste());
    }
}
