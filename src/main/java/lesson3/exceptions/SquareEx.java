package lesson3.exceptions;

import java.util.Scanner;

public class SquareEx {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int numberToSquare = 0;

        try {
            numberToSquare = s.nextInt();
            if (numberToSquare > 0) {
                System.out.println(Math.sqrt(numberToSquare));

            } else {
                throw new IllegalArgumentException("Number must be above 0!");
            }
        } catch (Exception e) {
            System.out.println("Wrong value, try again");
        }


    }
}
