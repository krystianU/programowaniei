package lesson3;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetEx {

    public static void main(String[] args) {
        TreeSet<String> zbiorDni = new TreeSet<>();
        zbiorDni.add("poniedziałek");
        zbiorDni.add("wtorek");
        System.out.println(zbiorDni);

        zbiorDni.add("wtorek");
        zbiorDni.add("wtorek");
        zbiorDni.add("wtorek");
        zbiorDni.add("środa");
        zbiorDni.add("czwartek");
        zbiorDni.add("piątek");
        zbiorDni.add("sobota");
        zbiorDni.add("niedziela");
        System.out.println(zbiorDni);

        for (String dzien : zbiorDni) {
            System.out.println(dzien);
        }
    }
}


