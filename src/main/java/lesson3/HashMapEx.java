package lesson3;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class HashMapEx {


    public static void main(String[] args) {
        Map<Integer, String> employees = new HashMap<>();
        Map<Integer, String> employees1 = new TreeMap<>();
        employees.put(1, "Aa Bb");
        employees.put(2, "Bb Cc");
        employees.put(3, "Cc Dd");
        employees.put(4, "Dd Ee");
        employees.put(5, "Ee Ff");
        employees.put(6, "Ff Gg");

        employees1.put(1, "Aa Bb");
        employees1.put(2, "Bb Cc");
        employees1.put(3, "Cc Dd");
        employees1.put(4, "Dd Ee");
        employees1.put(5, "Ee Ff");
        employees1.put(6, "Ff Gg");
    }
}
