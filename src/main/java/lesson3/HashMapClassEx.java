package lesson3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HashMapClassEx {
    public static void main(String[] args) {

        Map<String, String> teachers = new HashMap<>();
        teachers.put("Programowanie obiektowe", "Michal");
        teachers.put("Matematyka", "Magda");
        teachers.put("Informatyka", "Jarosław");

        System.out.println(teachers.get("Programowanie obiektowe"));
        Map<Integer, String> rooms = new HashMap<>();

        rooms.put(1, "Biologia");
        rooms.put(100, "Biologia");
        rooms.put(1000, "Biologia");
        rooms.put(150, "Biologia");
        rooms.put(2, "Matematyka");
        rooms.put(3, "Geografia");
        rooms.put(4, "WF");
        rooms.put(5, "WF");
        rooms.put(6, "Programowanie obiektowe");
        rooms.put(7, "Programowanie obiektowe");

        for(HashMap.Entry<Integer, String> entry : rooms.entrySet()){
            if (entry.getValue().equals("Biologia")){
                teachers.get(entry.getValue());
            }
        }

        ArrayList studentsList1 = new ArrayList();
        ArrayList studentsList2 = new ArrayList();

        studentsList1.add("Aa Bb");
        studentsList1.add("Bb Cc");
        studentsList1.add("Cc Dd");

        studentsList2.add("Dd Ee");
        studentsList2.add("Ee Ff");
        studentsList2.add("Ff Gg");

        ArrayList studentsList3 = new ArrayList();
        studentsList3.addAll(studentsList1);
        studentsList3.addAll(studentsList2);

        Map<Integer, ArrayList> studentGroups = new HashMap<>();
        studentGroups.put(12, studentsList1);
        studentGroups.put(22, studentsList2);
        studentGroups.put(32, studentsList3);

        System.out.println(studentGroups.get(32));
    }
}
