/*
Wygwiazdkuj linię o długości zadanej przez użytkownika.

*/

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int length = s.nextInt();

        for(int i = 0; i < length; i++){
            System.out.print("*");
        }
    }
}
